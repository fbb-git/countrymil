//                     usage.cc

#include "main.ih"

namespace
{

char part1[] = R"_( [options] countries
Where:
   [options] - optional arguments (short options between parentheses):
     --accept argument         - Accept email from a country otherwise
                                 rejected sent to e-mail address(es)
                                 specified by regexes. Format per line:
                                 "ISO-Code Regex(es)"
                                 Note: multiple regexes are OK, but only
                                 one country can be specified.
                                 Multiple -a options may be provided
     --config (-c) file        - Config file (may contain lines like the
                                 long options without the leading -- chars))
                                 Command line options overrrule config file
                                 options ()_";

char part2[] = R"_()
                                 When modifying the config file restart the
                                 program to activate the new configuration
     --database (-d) arch      - Archive containing the data of rejected 
                                 senders ()_";

char part3[] = R"_()
     --debug                   - Do not fork, show info to std error
     --expiration (-e) time    - Registration duration. Append d|h|m 
                                 for days/hours/minutes ()_"; 

char part4[] = R"_()
                                 See also --resend-within.
     --geoip-path (-g) path    - Path to the GeoIP database to use
                                 (default /usr/share/GeoIP/GeoIP.dat)
     --help (-h)               - Provide this help
     --logname (-l) file       - Logfile (not used with --debug)
                                 -l {stdout} logs to std out,
                                 -l {stderr} logs to std error
                                 ()_";

char part5[] = R"_()
     --logtypes (-L) type(s)   - Set the log-message type(s):
                                 a: accepted mail addresses
                                 d: decisions
                                 e: (non-fatal) errors
                                 h: (Received:) headers
                                 i: info
                                 m: db modifications
                                 s: settings
                                 w: warning ()_";

char part6[] = R"_()
     --nogo                    - Do not start the Milter, only perform
                                 startup actions (read database and IP-address
                                 ranges specified at -C
     --pidfile (-P) path       - File containing the daemon's PID
                                 ()_";

char part7[] = R"_()
     --protection (-p) mode    - Use <mode> as the socket mode (rwx-style
                                 or use an octal number (by default the mode
                                 set by the socket construction is used)
     --rewrite (-r) freq       - Database rewrite interval (append d|h|m for 
                                 an interval in days/hours/minutes. The
                                 minimum rewrite interval is 10m ()_";

char part9[] = R"_().
     --socket (-s) path        - Use <path> as the Unix domain socket
                                 ()_";

char part11[] = R"_()          
     --user (-u) name[:grp]    - Run as user `name', group `grp'.
                                 May also be uid:gid (current user:group)
     --version (-v)            - Show version information and terminate

   countries   - file containing the ws-separated 2-letter ISO codes
                 of countries from where e-mail should be rejected.)_";
}

void usage(std::string  const  &progname)
{
    cout << "\n" <<
    progname << " by Frank B. Brokken (f.b.brokken@rug.nl)\n" <<
    progname << " V" << version << " (" << year << ")\n"
    "\n"
    "Usage: " << progname << part1 << Config::defaultConfigName() <<
                            part2 << Config::defaultDbName() <<
                            part3 << Config::defaultExpiration() <<
                            part4 << Config::defaultLogName() << 
                            part5 << Config::defaultLogTypes() <<
                            part6 << Config::defaultPidName() <<
                            part7 << Config::defaultRewrite() <<
                            part9 << Config::defaultSocketName() <<
                            part11 << endl;
}

//char part8[] = R"_().
//     --reject cidr             - Reject e-mail if the headers contain
//                                 CIDR cidr (e.g., 10.10.0.0/16). The
//                                 /xx mask may be omitted if the address
//                                 is a stand-alone address
//     --resend-within (-R) time - Mail resend within the --resend-within time 
//                                 interval is considered non-spam and is
//                                 accepted. Append d|h|m for days/hours/minutes
//                                 ()_";  


