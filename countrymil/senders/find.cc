#define XERR
#include "senders.ih"

    // received recipient addresses are already using lc: no need to convert
bool Senders::find(string const &sender) const
{
    return d_recipients.find(sender) or AddressBase::find(sender);
}
