#ifndef INCLUDED_SENDERS_
#define INCLUDED_SENDERS_

#include <iosfwd>
#include "../addressbase/addressbase.h"

class Recipients;

class Senders: private AddressBase
{
    Recipients const &d_recipients;  // valid recipients are also valid senders

    public:
        Senders(Recipients const &recipients);
        void read();                // read the accepted sender addresses
        bool find(std::string const &sender) const;
        bool accept(std::string const &domain) const;
};

inline bool Senders::accept(std::string const &domain) const
{
    return AddressBase::accept(domain);
}

#endif

