#define XERR
#include "senders.ih"

void Senders::read()
{
    AddressBase::read("Senders:", config().senders());
}
