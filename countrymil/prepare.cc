#define XERR
#include "main.ih"

void prepare(ArgConfig &arg)
{
    arg.versionHelp(usage, version, 1);
    arg.setCommentHandling(ArgConfig::RemoveComment);    
    Config::initialize();
}
