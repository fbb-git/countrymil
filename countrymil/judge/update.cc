#define XERR
#include "judge.ih"

void Judge::update(bool *count, HostInfo &info)
{
    if (*count)
    {
        *count = false;
        ++info.count;
        info.time = time(0);
        d_modifiedHostMap = true;
    }
}
