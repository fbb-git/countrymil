#define XERR
#include "judge.ih"

void Judge::received(ID id, string const &hdrTxt)
{
    if (not config().logTypes().contains('h'))
        return;

    lock_guard lg{ d_mutex };

    log('i') << "Judge::received " << d_map[id].id << ": hdr = " << hdrTxt << 
                                                                        endl;
}
