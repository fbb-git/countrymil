#ifndef INCLUDED_JUDGE_
#define INCLUDED_JUDGE_

#include <mutex>

#include "../types/types.h"
#include "../configlog/configlog.h"

class CountryData;
class Accept;
class Recipients;
class Senders;

class Judge: private ConfigLog, public Types
{
    struct HostInfo                     // for invalid hosts
    {
        size_t count = 0;               // # of encounters
        size_t time  = 0;               // time of last encounter
    };
                                // last two components of the hostname
                                // or the first 2 components of an address
    using HostMap = std::unordered_map<std::string, HostInfo>;

    using ID = SMFICTX *;
    using StrSet = std::unordered_set<std::string>; // recipient(s)
    struct Data
    {
        Type action = CONTINUE;
        bool used = false;              // false: only connect requests
        bool count;                     // true: inc. HostInfo count at update
        std::string host;
        std::string sender;
        size_t id;
    };
    using IDMap = std::unordered_map<ID, Data>;

    CountryData &d_countryData;
    Accept &d_accept;
    Recipients &d_recipients;
    Senders &d_senders;

    size_t d_expiration;
    size_t d_rewriteSecs;
    size_t d_rewrite;                   // rewrite time of the .db file
    size_t d_now;                       // set by, e.g., connect
    size_t d_id;                        // rotates from 100 to 999
    IDMap d_map;                        // uses Data

    bool d_modifiedHostMap;
    HostMap d_hostMap;                  // HostInfo: info about invalid hosts

    std::mutex d_mutex;

    public:
        Judge(CountryData &countryData, Accept &d_accept, 
              Recipients &recipients, Senders &senders);
        void read();
        void write();                   // write the info of rejected hosts

        Type connect(ID id, std::string hostname);      // connecting host
                                                     // the mail-sender:
        Type sender(ID id, std::string const &from); // may add to d_hostMap
        Type recipient(ID id, std::string const &to);   // recipient(s)
        void received(ID id, std::string const &hdrTxt);
        Type close(ID id);

    private:
        bool expired(size_t hostTime) const;                        //  .ih
        void readHostInfo();
        void update(bool *count, HostInfo &info);

        static std::string family(std::string host);
};
        
#endif
