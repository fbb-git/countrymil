#define XERR
#include "judge.ih"

    // called only once. When that's altered, clear d_hostMap

void Judge::readHostInfo()
{
    string dbName = config().dbName();
    ifstream in{ dbName };

    string host;
    HostInfo info;

    d_now = time(0);
    string skip;
    while (in >> skip >> skip >> info.time >> info.count >> host)
    {
        in.ignore(1000, '\n');          // skip the rest of the line

        if (not expired(info.time))
            d_hostMap[host] = info;
    }

    log('s') << "Read " << d_hostMap.size() << 
                " rejected hosts from " << dbName << endl;
}
