#define XERR
#include "judge.ih"

    // verify that the host is not from a rejected country
Judge::Type Judge::connect(ID id, string host)
{
    bool address = host.front() == '[';

    if (address)        // received a bracketed address
        host.erase(0, 1).pop_back();

    bool accepted = d_senders.accept(host);

                                            // keep the host's used elements
    string group = accepted ? host : family(host);

    lock_guard lg{ d_mutex };

    if (d_map.find(id) != d_map.end())      // multiple connects at id ??
        log('e') << "Judge::connect " << nr(id) << 
                                    ": connect before close" << endl;

    Data &data = d_map[id];                 // the data of this connection
    data.count = true;
    data.host = group;
    if (++d_id > 999)
        d_id = 100;
    data.id = d_id;

    if (not accepted)
    {
                                            // an earlier encountered bad host
        if (auto iter = d_hostMap.find(group); iter != d_hostMap.end())
        {
            d_now = time(0);
            if (expired(iter->second.time))     // rm the host if expired
                d_hostMap.erase(iter);
            else                                // update the host's details
            {                                   // and REJECT
                update(&data.count, iter->second);
                log('d') << "Judge::connect " << data.id << " re-REJECT " << 
                            group << " (" << iter->second.count << ')' << endl;
                return data.action = REJECT;
            }
        }   
    }

    if (accepted)           // a host from an accepted domain
        log('i') << "Judge::connect " << data.id << " by " << host << endl;
    else if (               // a host not from a rejected country
        string iso = d_countryData.find(host, address); 
        iso.empty()
    )
        log('i') << "Judge::connect " << data.id << " by " << host << 
                    " (" << group << ')' << endl;
    else                    // reject if the host is fm. a rejected country
    {                 
        log('d') << "Judge::connect " << data.id << 
                    " REJECT " << host << " (" << iso << ')' << endl;
        data.action = REJECT;
    }

    return data.action;
}
