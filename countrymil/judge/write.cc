#define XERR
#include "judge.ih"

void Judge::write()
{
    string dbName = config().dbName();
    ofstream out{ dbName };

    d_now = time(0);

    size_t nExpired = 0;
    auto iter = d_hostMap.begin();
    while (iter != d_hostMap.end())
    {
        time_t secs = iter->second.time;
        if (expired(secs))
        {
            iter = d_hostMap.erase(iter);
            ++nExpired;
        }
        else
        {
            out << put_time( gmtime(&secs), "%y-%m-%d %H:%M:%S ") << 
                   secs << setw(4) << iter->second.count << ' ' <<
                   iter->first << '\n';
            ++iter;
        }
    }

    d_modifiedHostMap = false;

    log('s') << "Wrote " << d_hostMap.size() << 
                " rejected hosts to " << dbName << ", " << 
                nExpired << " expired hosts" << endl;
}


