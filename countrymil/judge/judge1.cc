#define XERR
#include "judge.ih"

Judge::Judge(CountryData &countryData, Accept &accept, 
             Recipients &recipients, Senders &senders)
:
    d_countryData(countryData),
    d_accept(accept),
    d_recipients(recipients),
    d_senders(senders),
    d_expiration(config().expiration()),    // bad host expirations seconds
    d_rewriteSecs(config().rewriteSecs()),  // seconds until the next rewrite
    d_rewrite(time(0) + d_rewriteSecs),     // time of the next rewrite
    d_id(999),
    d_modifiedHostMap(false)
{
    log('s') << "Next rewrite: " << 
                DateTime( d_rewrite, DateTime::LOCALTIME ) << endl;
}
