#define XERR
#include "judge.ih"

Judge::Type Judge::close(ID id)
{
    lock_guard lg{ d_mutex };

    auto iter = d_map.find(id);

    d_now = time(0);
    if (d_rewrite < d_now)          // beyond the rewrite interval: 
    {
        d_rewrite = d_now + d_rewriteSecs;  // set the next rewrite time
        if (d_modifiedHostMap)
            write();                        // write the updated database
        log('s') << "Next rewrite: " << 
                    DateTime( d_rewrite, DateTime::LOCALTIME ) << endl;
    }

    Type action = CONTINUE;

    if (iter == d_map.end())
        log('e') << "Judge::close " << nr(id) << ": undefined ID" << endl;
    else 
    {
        log('i') << "Judge::close " << iter->second.id;
        if (iter->second.used)
        {
            action = iter->second.action;
            log('i') << ". action: " << name(action) << endl;
        }
        else
        {
            log('i') << ": following connect: REJECT" << endl;
            update(&iter->second.count, d_hostMap[iter->second.host]);
            action = REJECT;
        }

        d_map.erase(iter);
    }
    
    return action;
}



