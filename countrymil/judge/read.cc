#define XERR
#include "judge.ih"

void Judge::read()
{
    d_countryData.read();
    d_accept.read();
    d_recipients.read();
    d_senders.read();

    readHostInfo();
}

//FBB    ID id = reinterpret_cast<ID>(123);
//FBB
//FBB    while (true)
//FBB    {
//FBB        cout << "host sender recipient ? ";
//FBB        string host, sendr, recip;
//FBB        if (not (cin >> host >> sendr >> recip))
//FBB            break;
//FBB        connect(id, host);
//FBB        sender(id, sendr);
//FBB        recipient(id, recip);
//FBB        close(id);
//FBB    }




