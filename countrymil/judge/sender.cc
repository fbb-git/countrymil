#define XERR
#include "judge.ih"

//  Sender: 
//      <>: add the host to d_hostMap and reject
//      registered sender:  CONTINUE
//      otherwise: data.action == CONTINUE ? CONTINUE : TEMPFAIL

Judge::Type Judge::sender(ID id, string const &from)
{
    lock_guard lg{ d_mutex };

    Data &data = d_map[id];
    data.used = true;

    log('i') << "Judge::sender " << data.id << ": sender " << from;

    if (from == "<>")                   // reject mail from undefined sender
    {
        HostInfo &info = d_hostMap[data.host];      // add the host
        update(&data.count, info);                  // update its details
        log('i') << ": REJECT " << data.host;
        data.action = REJECT;          
    }
    else
    {
        if (not d_senders.find(from))       // not a registered sender
            log('i') << " (not registered): " << name(data.action);
        else                                // a registered sender
        {
            log('i') << " (accept)";
            data.action = ACCEPT;
        }
        d_map[id].sender = from;
    }

    log('i') << endl;
    return data.action;
}
