#define XERR
#include "judge.ih"

// Recipient:
//      not registered: set the host in d_hostMap, and REJECT
//      registered: return the current action

Judge::Type Judge::recipient(ID id, string const &to)
{
    lock_guard lg{ d_mutex };

    auto &data = d_map[id];
    data.used = true;

    if (not d_recipients.find(to))              // not a registered recipient
    {
        log('i') << "Judge::recipient " << data.id << ": " << to << 
                    " not registered: REJECT" << endl;
        update(&data.count, d_hostMap[data.host]);
        data.action = REJECT;
    }
    else        
        log('i') << "Judge::recipient " << data.id << " to " << to << 
                    ": " << name(data.action) << endl;

    return data.action;
}
