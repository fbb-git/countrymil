#define XERR
#include "judge.ih"

// static
string Judge::family(string host)
{
    if (isdigit(host.back()))               // rm the final address nr:
        host.resize(host.rfind('.'));       // 1.2.3.4 -> 1.2.3
    else                                    // or use the last 2 words of the
    {                                       // name: a.b.c -> c.b
        String::Type type;
        auto words = String::split(&type, host, String::TOK, ".");

        host = words.back();                // start at the last component

        for (
            size_t from = words.size() - 1, to = words.size() > 2;
            from-- != to;
        )
            (host += '.') += words[from];
    }

    return host;
}
