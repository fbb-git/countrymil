#ifndef INCLUDED_ACCEPT_
#define INCLUDED_ACCEPT_

#include <vector>
#include <unordered_map>
#include <string>

#include <bobcat/pattern>

class CountryData;

#include "../configlog/configlog.h"

// E.g., accept GR  hein@

class Accept: private ConfigLog
{
    using PatternVect = std::vector<FBB::Pattern>;

                                    // iso
    using Map = std::unordered_map<std::string, PatternVect>;

    CountryData const &d_countryData;

    Map d_map;


    public:
        Accept(CountryData const &countryData);
        void read();                // (re)read always accepted destinations

    private:
        void add(std::string const &accept);
};
        
#endif
