#define XERR
#include "accept.ih"

void Accept::add(string const &accept)
{
    istringstream in(accept);

    string iso;

    if (not (in >> iso) or iso.length() != 2)       // read the ISO code
    {
        log('e') << accept << ": missing/invalid country specification" << 
                                                                        endl;
        return;
    }
                                                    // ignore if not a 
    if (not d_countryData.rejected(iso))            // rejected ISO code
    {
        log('w') << "[warning] `" << iso << "' in `" << 
                                    accept << "' is not rejected" << endl;
        return;
    }

    PatternVect &patterns = d_map[iso];             // add the pattern(s)
    string pattern;
    while (in >> pattern)
    {
        log('a') << "mail to: `" << pattern << "' for mail from " <<
                                                    iso << " is OK" << endl;
        patterns.push_back(Pattern{ pattern });
    }
}
