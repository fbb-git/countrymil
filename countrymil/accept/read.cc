#define XERR
#include "accept.ih"

void Accept::read()
{
    d_map.clear();

    for (string const &accept: config().accept())
        add(accept);

    log('a') << "Accept: " << d_map.size() << " accept specifications" <<
                                                                        endl; 
}
