#include "config.ih"

void Config::read(vector<string> &dest, char const *regex)
{
    dest.clear();

    auto iters = d_argconf.beginEndRE(regex);

    for (auto &line: ranger(iters.first, iters.second))
        addValue(line, dest);
}


