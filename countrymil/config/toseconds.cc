#define XERR
#include "config.ih"

size_t Config::toSeconds(char const *label, std::string txt) const
{
    istringstream istr{ txt };

    size_t ret;
    if (not (istr >> ret))
        throw txt + "': no number";

    string name;
    char unit = 0;
    istr >> unit;               // no unit: unit at 0 -> exception below
    switch (unit)
    {
        case 'd':
            ret *= 3600 * 24;
            name = " days";
        break;

        case 'h':
            ret *= 3600;
            name = " hours";
        break;

        case 'm':
            ret *= 60;
            name = " minutes";
        break;

        default:
        break;
    }

    if (name.empty())
        throw txt + "': invalid time specification";


    size_t length = txt.length();

    txt.replace(length - 1, length, name);  // replace 'dhm' by its name

    (*d_logPtr)('s') << label << " `" << txt << '\'' << ": " << ret << endl;

    return ret;
}




