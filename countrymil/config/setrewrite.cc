#include "config.ih"

void Config::setRewrite()
{
    if (not d_argconf.option(&d_rewrite, 'r'))
        d_rewrite = s_rewrite;
    d_rewriteSecs = toSeconds("rewrite", d_rewrite);

    if (d_rewriteSecs < 600)                    // interval < 10m
    {
        d_rewriteSecs = 600;
        d_rewrite = "10m";
    }
}
