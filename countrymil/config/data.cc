#include "config.ih"

Config *Config::s_config;

char const Config::s_configName[]   = "/etc/countrymil/countrymil.cf";
char const Config::s_dbName[]       = "/etc/countrymil/countrymil.db";
char const Config::s_expiration[]   = "24h";
char const Config::s_geoIPpath[]    = "/usr/share/GeoIP/GeoIP.dat";
char const Config::s_logName[]      = "/var/log/countrymil.log";
char const Config::s_pidName[]      = "/var/run/countrymil.pid";
char const Config::s_rewrite[]      = "24h";
char const Config::s_socketName[]   = "/var/run/countrymil.sock";
char const Config::s_logTypes[]     = "deimsw";
