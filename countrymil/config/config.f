inline Config::StringVect const &Config::accept() const
{
    return d_accept;
}

inline Config::StringVect const &Config::recipients() const
{
    return d_recipients;
}

inline Config::StringVect const &Config::senders() const
{
    return d_senders;
}

inline std::string const &Config::dbName() const
{
    return d_dbName;
}

inline char const *Config::defaultExpiration() 
{
    return s_expiration;
}

inline size_t Config::expiration() const
{
    return d_expiration;
}

inline char const *Config::defaultRewrite() 
{
    return s_rewrite;
}

inline std::string const &Config::rewrite() const
{
    return d_rewrite;
}

inline size_t Config::rewriteSecs() const
{
    return d_rewriteSecs;
}


inline std::string const &Config::geoIPpath() const
{
    return d_geoIPpath;
}

inline std::string const &Config::user() const
{
    return d_newUser;
}

inline std::string const &Config::logName() const
{
    return d_logName;
}

inline char const *Config::defaultGeoIPpath() 
{
    return s_geoIPpath;
}

inline char const *Config::defaultLogName() 
{
    return s_logName;
}

inline std::string const &Config::pidName() const
{
    return d_pidName;
}

inline char const *Config::defaultPidName() 
{
    return s_pidName;
}

inline std::string const &Config::protection() const
{
    return d_protection;
}

inline std::string const &Config::socketName() const
{
    return d_socketName;
}

inline char const *Config::defaultSocketName() 
{
    return s_socketName;
}

inline char const *Config::defaultConfigName() 
{
    return s_configName;
}

inline std::string const &Config::configName() const
{
    return d_configName;
}

inline char const *Config::defaultDbName() 
{
    return s_dbName;
}

inline std::string const &Config::logTypes() const
{
    return d_logTypes;
}

inline char const *Config::defaultLogTypes() 
{
    return s_logTypes;
}

inline char const *Config::countries() const
{
    return d_argconf[0];
}

inline bool Config::debug() const
{
    return d_debug;
}

inline bool Config::nogo() const
{
    return d_nogo;
}
