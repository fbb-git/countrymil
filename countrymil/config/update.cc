#define XERR
#include "config.ih"

void Config::update()
{
                                                    // accepted senders from
                                                    // otherwise rejected
                                                    // countries for specific
    read(d_accept,      R"(^\s*accept\s)");         // recipients

                                                    // defined recipients (may
    read(d_recipients,  R"(^\s*recipients\s)");     // also send mail)

                                                    // defined senders (in 
    read(d_senders,     R"(^\s*senders\s)");        // addition to recipients)
}

    
