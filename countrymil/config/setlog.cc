#define XERR
#include "config.ih"

void Config::setLog()
{
    d_logName = s_logName;                      // try the default logname

    if (d_argconf.option(0, "debug"))           // use stderr for debugging
        d_logName = "&2";
    else if (d_argconf.option(&d_logName, 'l')) // or use as specified
    {
        if (d_logName == "{stdout}")
            d_logName = "&1";
        else if (d_logName == "{stderr}")
            d_logName = "&2";
    }

    d_logPtr = &Log::initialize(d_logName);
    d_logPtr->str(d_logTypes);

    (*d_logPtr)('i') << d_argconf.basename() << " starts." << endl;
}





