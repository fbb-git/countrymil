#include "config.ih"

Config &Config::instance()
{
    if (!s_config)
        throw Exception() << "Config::instance(): Config not yet initialized";

    return *s_config;
}
