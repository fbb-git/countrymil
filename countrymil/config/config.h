#ifndef _INCLUDED_CONFIG_
#define _INCLUDED_CONFIG_

#include <string>
#include <vector>
#include <bobcat/argconfig>
#include <bobcat/stat>

namespace FBB
{
    class Log;
}

struct Config
{
    using StringVect = std::vector<std::string>;

    private:
        static Config *s_config;
    
        FBB::ArgConfig &d_argconf;
        FBB::Log *d_logPtr;
    
        StringVect d_accept;      // specified in the config file
        StringVect d_recipients;
        StringVect d_senders;

        bool d_debug;
        bool d_nogo;
    
        std::string d_configName;
        std::string d_dbName;
        std::string d_geoIPpath;
        std::string d_logName;
        std::string d_newUser;
        std::string d_pidName;
        std::string d_protection;
        std::string d_rewrite;
        std::string d_socketName;
        std::string d_logTypes;
        
        FBB::Stat d_configStat;

        size_t d_expiration;                // expiration time in seconds
        size_t d_rewriteSecs;

        static char const s_configName[];
        static char const s_dbName[];
        static char const s_expiration[];
        static char const s_geoIPpath[];
        static char const s_logName[];
        static char const s_pidName[];
        static char const s_socketName[];
        static char const s_rewrite[];
        static char const s_logTypes[];
    
    public:
        static Config &initialize();    // creates Config if not existing,
                                        // does init() 

        static Config &instance();      // returns Config if available.

        std::string const &configName() const;
        std::string const &dbName() const;
        std::string const &geoIPpath() const;
        std::string const &logName() const;
        std::string const &logTypes() const;
        std::string const &pidName() const;
        std::string const &protection() const;
        std::string const &rewrite() const;
        std::string const &socketName() const;
        std::string const &user() const;

        char const *countries() const;
        bool debug() const;
        bool nogo() const;

        StringVect const &accept() const;                               // .f
        StringVect const &recipients() const;                           // .f
        StringVect const &senders() const;                             // .f

        static char const *defaultExpiration();             // d, h, m
        static char const *defaultLogName();
        static char const *defaultPidName();
        static char const *defaultDbName();
        static char const *defaultSocketName();
        static char const *defaultConfigName();
        static char const *defaultGeoIPpath();
        static char const *defaultRewrite();                // d, h, m
        static char const *defaultLogTypes();

        size_t expiration() const;
        size_t rewriteSecs() const;

    private:
        size_t toSeconds(char const *label, std::string txt) const;

        void update();                  // update update-able values

        void read(std::vector<std::string> &dest, char const *regex);

        static void addValue(std::string const &line, 
                             std::vector<std::string> &dest);
        Config();

        void setExpiration();
        void setLog();
        void setLogTypes();
        void setRewrite();
};

#include "config.f"

#endif
