#define XERR
#include "config.ih"

Config::Config()
:
    d_argconf(ArgConfig::instance()),

    d_debug(d_argconf.option(0, "debug")),      // these are only avail.
    d_nogo(d_argconf.option(0, "nogo"))         // as cmd-line options
{
    if (not d_argconf.option(&d_configName, 'c'))
        d_configName = s_configName;

    // d_configStat.set(d_configName);          // load the config file
    d_argconf.open(d_configName);

    if (not d_argconf.option(&d_dbName, 'd'))
        d_dbName = s_dbName;

    if (not d_argconf.option(&d_geoIPpath, 'g'))
        d_geoIPpath = s_geoIPpath;

    setLogTypes();
    setLog();           // this inserts a log, so after setLogTypes

    setExpiration();

    d_argconf.option(&d_protection, 'p');

    if (not d_argconf.option(&d_pidName, 'P'))
        d_pidName = s_pidName;

    setRewrite();

    if (not d_argconf.option(&d_socketName, 's'))
        d_socketName = s_socketName;

    d_argconf.option(&d_newUser, 'u');

    update();                       // read the acept .. senders specs

    (*d_logPtr)('s') << "Using archive `" << d_dbName << "'" << endl;
}


