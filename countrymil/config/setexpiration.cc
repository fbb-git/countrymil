#define XERR
#include "config.ih"

void Config::setExpiration()
{
    string value;
    if (not d_argconf.option(&value, 'e'))
        value = s_expiration;
    d_expiration = toSeconds("expiration", value);
}

