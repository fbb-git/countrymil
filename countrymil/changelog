countrymil (4.01.00)

  * Prevented double counting of rejected hosts

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 26 Dec 2023 12:12:58 +0100

countrymil (4.00.00)

  * The class Database received a complete overhaul

  * Using unordered_map/set instead of ordered map/set

  * Single-letter options for --accept, --debug, --domain, --envelope, and
    --reject were removed.

countrymil (3.03.00)

  * Added configuration option 'discard-unknown' to discard mails receiveed
    from undefined users

  * Changed the GNU AFFERO GENERAL PUBLIC LICENSE to the GNU GENERAL PUBLIC
    LICENSE in the file LICENSE

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 14 Nov 2023 12:06:57 +0100

countrymil (3.02.03)

  * Removed the COMPILER string from INSTALL.im

  * Requires bobcat >= 5.00.00

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 24 Apr 2019 12:54:41 +0200

countrymil (3.02.02)

  * Cleanup of icmake files, and updating to icmake 8.00.00.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 05 Dec 2015 11:20:58 +0100

countrymil (3.02.01)

  * Using precompiled headers

  * Slightly reorganized the icmake/ files.

  * Adapted sources to libbobcat4

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 04 Oct 2015 09:01:54 +0200

countrymil (3.02.00)

  * Milter functions overridden by MoMilter show up in the logs as being
    called up to log level 1.

  * Countrymil now correctly checks accepted envelopes: specified envelopes
    must completely be present in actual envelopes. 

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 25 Oct 2013 11:21:01 +0200

countrymil (3.01.00)

  * Added option --reject, unconditionally rejecting e-mail if a  Received
    header contains an address within a specified CIDR range.

  * In preparation for Bobcat 3.12.00, changed catching Errnos to
    std::exceptions. 

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Thu, 14 Mar 2013 18:47:50 +0100

countrymil (3.00.00)

  * Switched to using GeoIP instead of ip_to_country.cvs

  * Added option --envelope, unconditionally accepting e-mail having specific
    envelopes. Multiple --envelope options may be specified.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 29 Jun 2012 05:31:25 +0200

countrymil (2.02.00)

  * Adapted to Bobcat >= 3.00.00

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 04 May 2012 14:44:34 +0200

countrymil (2.01.01) unstable; urgency=low

  * database/writearchiveline.cc's function call no longer uses 'unsigned'
    but 'size_t' to correspond with database/database.h. Required by adm64
    archs.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 06 Sep 2011 20:57:36 +0200

countrymil (2.01.00) unstable; urgency=low

  * Replacing Msg::open by Errno::open, using bobcat 2.09.00
  
  * Replacing FnWrap1c by FnWrap::unary

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 29 Oct 2010 14:36:13 +0200

countrymil (2.00.2) unstable; urgency=low

  * Added missing std=c++0x to compiler call in icmake/manual

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 03 Feb 2010 09:56:11 +0100

countrymil (2.00.1) unstable; urgency=low

  * Changes documented  SVN archive, date below is fake

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 02 Dec 2009 11:51:39 +0100

countrymil (1.00.00) unstable; urgency=low

  * Removed argconf class, using FBB::ArgConfig instead

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 02 Dec 2008 11:51:39 +0100

countrymil (0.00) unstable; urgency=low

  * Initial Release.

 -- Frank B. Brokken <f.b.brokken@rc.rug.nl>  Wed, 01 Oct 2008 21:10:13 +0200
