#include <iostream>
#include <GeoIP.h>
using namespace std;

// link to GeoIP

int main(int argc, char **argv)
{
    GeoIP * d_geoIP =
        GeoIP_open("/usr/share/GeoIP/GeoIP.dat", GEOIP_MEMORY_CACHE);

    for (int idx = 1; idx != argc; ++idx)
    {
        char const *ptr = GeoIP_country_code_by_addr(d_geoIP, argv[idx]);

        if (ptr == 0)
            ptr = GeoIP_country_code_by_name(d_geoIP, argv[idx]);

        cout << argv[idx] << ": " << 
            (ptr == 0 ? "not found" : ptr) << '\n';
    }
                
}







