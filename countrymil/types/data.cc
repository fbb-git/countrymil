#include "types.ih"

// static
char const *Types::s_type[] =
{
    "ACCEPT",           // see types.h: sequence must match 
    "CONTINUE",         //              Types::Type's sequence
    "DISCARD",
    "REJECT",
    "TEMPFAIL"
};
