#ifndef INCLUDED_TYPES_
#define INCLUDED_TYPES_

#include <libmilter/mfapi.h>

#include <unordered_map>
#include <unordered_set>
#include <string>

class Types
{
    static char const *s_type[];

    public:
        enum Type
        {
            ACCEPT,                         // see momilter/data.cc
            CONTINUE,
            DISCARD,
            REJECT,
            TEMPFAIL,
        };
    
        template <typename Type>            // only the SMFICTX specialization
        static size_t nr(Type type);        // is defined

        static char const *name(Type type);
};

// static
inline char const *Types::name(Type type)
{
    return s_type[type];
}

template <>
inline size_t Types::nr<>(SMFICTX *id)
{
    return reinterpret_cast<size_t>(id) % 1000;
}

#endif
