#ifndef INCLUDED_RECIPIENTS_
#define INCLUDED_RECIPIENTS_

#include <iosfwd>
#include "../addressbase/addressbase.h"

struct Recipients: private AddressBase
{
    Recipients() = default;
    void read();                // read the accepted recipients addresses
    bool find(std::string const &recipient) const;
};

inline bool Recipients::find(std::string const &recipient) const
{
    return AddressBase::find(recipient);
}
       
#endif


