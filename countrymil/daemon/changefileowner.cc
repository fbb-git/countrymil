#include "daemon.ih"

void Daemon::changeFileOwner(string const &filename,
                            string const &newUser, size_t uid, size_t gid)
{
    if (not config().debug() and chown(filename.c_str(), uid, gid) != 0) 
        log('e') << "[ERROR] can't chown " << newUser << filename << endl;
}
