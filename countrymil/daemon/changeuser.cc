#include "daemon.ih"

void Daemon::changeUser() 
try
{
    if (config().debug())
        return;

    string const &newUser(config().user());

    if (newUser.empty())
        return;

    istringstream istr(newUser);                 // prepare numerical extracts

    size_t uid;
    size_t gid = ~0U;

    if (istr >> uid)                            // extract uid
    {
        char dot;
        if (!(istr >> dot >> gid))              // and maybe gid
            gid = ~0U;
    }   
    else                                        // no uid: look it up
    {                                           // determine the user name
        string::size_type idx = newUser.find_first_of(".");
        string user;
        if (idx == string::npos)                // found name.[group]
            user = newUser;
        else
        {
            string group = newUser.substr(idx + 1);// determine the group
            struct group *gptr = 0;
            if (!(gptr = getgrnam(group.c_str())))  // determine the group
                throw newUser;                    
            gid = gptr->gr_gid;                     // and its id
            user = newUser.substr(0, idx);         // determine the user
        }
            
        struct passwd *pwd = getpwnam(user.c_str());// determine uid
        if (!pwd)
            throw newUser;
        uid = pwd->pw_uid;                          // and set its value
    }

    changeFileOwner(config().socketName(),          // change the socket's 
                    newUser, uid, gid);             // owner
                                                        
    changeFileOwner(config().dbName(),              // change the database's
                    newUser, uid, gid);             // owner
    
    if ((gid != ~0U && setgid(gid)) || setuid(uid)) // change uid.gid
        throw newUser;
}
catch (string const &user)
{
    throw Exception() << "Daemon::changeUser(): can't change "
                            "user[.group] to `" << user << '\'';
}
