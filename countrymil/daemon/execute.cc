#define XERR
#include "daemon.ih"

void Daemon::execute()
{
    if (config().debug())
        runMom();
    else
        fork();
}
