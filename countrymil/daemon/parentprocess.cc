#include "daemon.ih"

void Daemon::parentProcess()
{
    string const &pidName = config().pidName();

    ofstream pidFile(pidName.c_str());
    if (!pidFile)
    {
        log('w') << "[warning] can't write `" << 
                                                pidName << '\'' << endl;
        return;
    }
    pidFile << pid() << endl;
    log('s') << "Wrote `" << pidName << "' containing " << 
                                                pid() << endl;
}
