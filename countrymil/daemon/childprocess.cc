#include "daemon.ih"

void Daemon::childProcess()
{
    prepareDaemon();                // new

    log('s') << "Daemon::childProcess() starts" << endl;

    int ret = runMom();

    error_code ec;

    fs::remove(config().socketName(), ec);
    fs::remove(config().pidName());

    log('s') << "Daemon::childProcess() ends with " << ret << endl;

    throw ret;
}
