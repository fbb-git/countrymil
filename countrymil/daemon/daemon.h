#ifndef _INCLUDED_DAEMON_
#define _INCLUDED_DAEMON_

//#include <memory>

#include <bobcat/fork>
//#include <bobcat/signal>

#include "../configlog/configlog.h"

struct Daemon: private ConfigLog, public FBB::Fork
{
    Daemon() = default;
    ~Daemon() override;

    void execute();

    private:
        void changeUser();
        void defineLog();
        int runMom();
        void changeFileOwner(std::string const &filename,
                        std::string const &newUser, size_t uid, size_t gid);

        virtual void parentProcess();
        virtual void childProcess();        // calls runMom
};

#endif









