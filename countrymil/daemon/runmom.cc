#define XERR
#include "daemon.ih"

int Daemon::runMom()
try
{
            // These objects are defined here and not nested in their
            // receiving classes because the milter redefines the MoMilter
            // object at each connection. The Judge uses a mutex to prevent
            // access to its id-map when another connection starts in
            // parallel. 
    CountryData countryData;
    Accept accept{ countryData };   // stores the always accepted destinations
    Recipients recipients;
    Senders senders{ recipients };

    Judge judge{ countryData, accept, recipients, senders };
    judge.read();

    MoMilter mom{ judge };

    if (config().nogo())
        return 1;

    Milter::initialize("countrymil", mom,
                        Milter::CONNECT | Milter::SENDER |  // CLOSE is 
                        Milter::RECIPIENT | Milter::HEADER  // implied
                      );

    mom.makeSocket();

    changeUser();
    int ret = Milter::start() == false;    // OK returns 0 so main() returns 0

    judge.write();                          // write the rejected hosts info

    return ret;
}
catch (exception const &e)
{
    cout << e.what() << endl;
    return 1;
}
catch (int i)
{
    return i;
}


