#define XERR
#include "addressbase.ih"

    // from: the originating domain name

bool AddressBase::accept(string const &host) const
{
    size_t length = host.length();

    return
        find_if(d_domains.begin(), d_domains.end(),
            [&](pair<bool, string> const &domain)   // 1st true: domain with .
            {
                size_t pos = domain.second.rfind(host);  
            
                return
                    pos == 0                        // host must be in domain
                    or
                    (                               // or host contains domain
                        (pos = host.rfind(domain.second)) != string::npos
                        and domain.first            // and domain has as dot
                        and host[pos - 1] == '.'    // and a . at host[pos-1]
                        and                         // and domain at host's
                            pos + domain.second.length()        //      end
                            == length      
                    );
            }
        )
        != d_domains.end();
}
