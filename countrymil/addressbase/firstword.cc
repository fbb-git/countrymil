#define XERR
#include "addressbase.ih"

bool AddressBase::firstWord(istream &in, istream &istr)
{
    StringVect domains{ domainNames(istr) };

    if (domains.empty())
        return false;

    string line;
    String::Type type;

    while (getline(in, line))           // read the addresses fm the file
    {
        line = String::trim(line);

        if (line.front() == '#' or line.empty())    // comment-line
            continue;

                                                    // the :-separated words
        StringVect words = String::split(&type, line, String::TOK, ":");
        if (words.back() != "/nologin")
        {
            for (string const &domain: domains)
                insert( '<' + words.front() + '@' + domain + '>');
        }
    }

    return true;
}


