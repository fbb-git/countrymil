#define XERR
#include "addressbase.ih"

// e.g., 'senders /var/list/americans', receiving each filename in turn

void AddressBase::add(string const &spec)
{
    istringstream istr{ spec };
    char mode;
    string filename;
    istr >> mode >> filename;  

    ifstream in{ filename };
    if (not in)                     // no file
    {
        log('e') << "Can't read addresses file `" << filename << '\'' << endl;
        return;
    }

    switch (mode)
    {
        case 'A':       // lines with complete <addresses>, no domains needed
            hasAddress(in);
        return;

        case 'B':       // complete addresses, no domains needed, add <>
            addBrackets(in);
        return;

        case 'D':       // domain specifications like google.com
            addDomains(in);
        return;

        case 'F':       // 1st :-ending words contains addresses, 
                        // domains and  <> needed
            if (not firstWord(in, istr))
                break;
        return;

        case 'U':       // merely usernames, domains needed
            if (not userNames(in, istr))
                break;
        return;

        default:
            log('e') << "Mode " << mode << " in " << spec << 
                                                " not available" << endl;
        return;
    }

    log('e') << "In " << spec << " no domains specified" << endl;
}








