#define XERR
#include "addressbase.ih"

void AddressBase::hasAddress(istream &in)
{
    string line;
    while (getline(in, line))           // read the addresses fm the file
    {
        line = String::trim(line);

        if (line.front() == '#' or line.empty())    // comment-line
            continue;

        if (s_address << line)                      // found an address
            insert(s_address.matched());
    }
}


