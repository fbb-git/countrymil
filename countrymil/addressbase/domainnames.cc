#define XERR
#include "addressbase.ih"

AddressBase::StringVect AddressBase::domainNames(istream &istr)
{
    StringVect domains;
    copy(istream_iterator<string>{ istr }, istream_iterator<string>{},
         back_inserter(domains));

    return domains;
}
