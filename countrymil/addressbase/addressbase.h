#ifndef INCLUDED_ADDRESSBASE_
#define INCLUDED_ADDRESSBASE_

//  handle address specifications:
//
//  no domain: full address is provided. Otherwise
//  write the @ between the address and the domain
//  (domain.empty() ? "" : "@") + domain + '>');     

#include <iosfwd>
#include <unordered_set>
#include <string>
#include <vector>

#include "../configlog/configlog.h"

namespace FBB
{
    class Pattern;
}

struct AddressBase: protected ConfigLog
{
    using Addresses = std::unordered_set<std::string>;
    using StringVect = std::vector<std::string>;
    using BSVect = std::vector<std::pair<bool, std::string>>;

    private:
        char const *d_label;                // set by read.cc
        Addresses d_addresses;
        BSVect d_domains;

        static FBB::Pattern s_address;      // matches <...> address specs.

    protected:
        void read(char const *label, StringVect const &specs);

        Addresses const &addresses() const;
                                            // literal domains if not starting 
                                            // at . otherwise also subdomains
        bool accept(std::string const &domain) const;       
        bool find(std::string const &address) const;        // inline?
 
    private:
        void add(std::string const &filename);

        void addBrackets(std::istream &in);     // need to add < and >
        void addDomains(std::istream &in);
        bool firstWord(std::istream &in,        // addresses fm /etc/passwd
                       std::istream &domains);

        bool userNames(std::istream &in,        // mere usernames
                       std::istream &domains);

        void pwAddress(std::istream &in,        // addresses fm /etc/passwd
                       std::istream &domains);
        void hasAddress(std::istream &in);      // <address> in lines

        void insert(std::string const &line);
        StringVect domainNames(std::istream &in);
};

inline AddressBase::Addresses const &AddressBase::addresses() const
{
    return d_addresses;
}
 
        
#endif
