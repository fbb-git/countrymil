#define XERR
#include "addressbase.ih"

    // received addresses are already using lc: no need to convert
bool AddressBase::find(string const &host) const
{
    return d_addresses.find(host) != d_addresses.end();
}
