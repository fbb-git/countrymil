#define XERR
#include "addressbase.ih"

void AddressBase::addDomains(istream &in)
{
    string domain;
    while (in >> domain)           // read the addresses fm the file
    {
        in.ignore(1000, '\n');      // skip the rest of the address

        if (domain.front() != '#')  // not a comment-line: add the domain
        {
            bool dot = domain.front() == '.';
            if (dot)
                domain.erase(0, 1);

            d_domains.push_back({ dot, String::lc(domain) }); 
            log('a') << d_label << " domain `" << (dot ? "." : "") << 
                                                    domain << '\'' << endl;
        }
    }
}


