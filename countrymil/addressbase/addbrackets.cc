#define XERR
#include "addressbase.ih"

void AddressBase::addBrackets(istream &in)
{
    string address;
    while (in >> address)           // read the addresses fm the file
    {
        in.ignore(1000, '\n');      // skip the rest of the address

        if (address.front() != '#') // comment-address
            insert( '<' + address + '>');
    }
}


