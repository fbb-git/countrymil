#define XERR
#include "addressbase.ih"

void AddressBase::insert(string const &line)
{
    if (                                    // else insert the lc address
        auto ret = d_addresses.insert(String::lc(line)); 
        ret.second
    )
        log('a') << d_label << " `" << *ret.first << '\'' << endl;
}
