#define XERR
#include "addressbase.ih"

bool AddressBase::userNames(istream &in, istream &istr)
{
    StringVect domains{ domainNames(istr) };

    if (domains.empty())
        return false;

    string name;

    while (getline(in, name))           // read the addresses fm the file
    {
        name = String::trim(name);

        if (name.front() == '#' or name.empty())    // comment-name
            continue;

        for (string const &domain: domains)
            insert( '<' + name + '@' + domain + '>');
    }

    return true;
}


