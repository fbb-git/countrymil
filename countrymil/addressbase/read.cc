#define XERR
#include "addressbase.ih"

void AddressBase::read(char const *label, StringVect const &specs)
{
    d_label = label;
    d_addresses.clear();

    for (string const &spec: specs)
        add(spec);

    log('a') << label << ' '  << d_addresses.size() << 
                                        " address specifications" << endl; 
}
