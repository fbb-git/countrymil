#ifndef INCLUDED_COUNTRYDATA_
#define INCLUDED_COUNTRYDATA_

#include <GeoIP.h>

#include <string>
#include <unordered_set>

#include <bobcat/stat>

#include "../configlog/configlog.h"

    // when receiving mail from a sender, check whether the mail
    // came from an rejected country. If so, DISCARD the mail

class CountryData: private ConfigLog
{
    GeoIP *d_geoIP;
    std::string d_geoIPpath;
    FBB::Stat d_geoIPstat;
                                                    // ISO codes of rejected 
    std::unordered_set<std::string> d_rejected;     // countries

    public:
        CountryData();

        void read();                // (re)read the countries, 

                                    // accepted to-
                                    // addresses and from-envelopes

                                    // returns "" or the host's country code
        std::string find(std::string host, bool address);

        bool rejected(std::string const &iso) const;    // true: rejected   .f

    private:
        void loadGeoIP();

        void inspectGeoIP();        // called by find, reloading the data if
                                    // more recent data are availablw
};

#include "countrydata.f"
        
#endif





