#include "countrydata.ih"

void CountryData::inspectGeoIP()
{
    Stat geoIPstat(d_geoIPpath);

    if (geoIPstat.lastModification() != d_geoIPstat.lastModification())
    {
        log('s') << "Refreshing the GeoIP data base `" << 
                                                d_geoIPpath << '\'' << endl;
        GeoIP_delete(d_geoIP);
        loadGeoIP();
    }
}
