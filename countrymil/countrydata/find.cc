#include "countrydata.ih"

    // use the host info to check whether its origin is a rejected country

string CountryData::find(string host, bool address)
{
    inspectGeoIP();

    char const *iso = address ?
                    GeoIP_country_code_by_addr(d_geoIP, host.c_str())
                :
                    GeoIP_country_code_by_name(d_geoIP, host.c_str());

    if (iso != 0)
    {
        auto iter = std::find(d_rejected.begin(), d_rejected.end(), iso);

                                // host is/isn't from a rejected country
        return iter != d_rejected.end() ? *iter : "";
    }

    if (host != "localhost")
        log('i') << "Host `" << host << "' not in the GeoIP database" << endl;
    return "";
}




