inline bool CountryData::rejected(std::string const &iso) const
{
    return d_rejected.find(iso) != d_rejected.end();
}
