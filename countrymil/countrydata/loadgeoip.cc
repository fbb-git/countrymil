#include "countrydata.ih"

void CountryData::loadGeoIP()
{
    d_geoIP = GeoIP_open(d_geoIPpath.c_str(), GEOIP_MEMORY_CACHE);

    if (d_geoIP == 0)
        throw Exception{} << "CountryData::CountryData(): can't open `" << 
                            d_geoIPpath << '\'';

    log('s') << "GeoIP path is `" << d_geoIPpath << '\'' << endl;
    d_geoIPstat.set(d_geoIPpath);
}
