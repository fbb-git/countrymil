#include "countrydata.ih"

void CountryData::read()
{
    d_rejected.clear();                        // clear the previous set

    string const &name = config().countries();

    ifstream in{ Exception::factory<ifstream>
                    (
                        name,
                        ios::in, ios::in | ios::trunc
                    ) 
                };

    log('s') << "reading ISO codes of rejected countries from `" << 
                  name << '\'' << endl;

    for_each(istream_iterator<string>(in), istream_iterator<string>(),
        [&](string const &iso)
        {
            d_rejected.insert(iso);
        }
    );

    log('s') << d_rejected.size() << " rejected countries" << endl;
}
