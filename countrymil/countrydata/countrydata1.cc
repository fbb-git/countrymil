#include "countrydata.ih"

CountryData::CountryData()
:
    d_geoIP(0),
    d_geoIPpath(config().geoIPpath())
{
    loadGeoIP();
}
