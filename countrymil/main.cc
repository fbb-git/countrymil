#define XERR
#include "main.ih"

namespace
{
    ArgConfig::LongOption longOptions[] =
    {
        ArgConfig::LongOption( "accept",            Arg::Required),
        ArgConfig::LongOption( "config",            'c'),
        ArgConfig::LongOption( "database",          'd'),
        ArgConfig::LongOption( "debug"                 ),
        ArgConfig::LongOption( "discard-unknown"       ),
        ArgConfig::LongOption( "domain",            Arg::Required), 
        ArgConfig::LongOption( "expiration",        'e'),       
        ArgConfig::LongOption( "geoip-path",        'g'),
        ArgConfig::LongOption( "help",              'h'),
        ArgConfig::LongOption( "logname",           'l'),
        ArgConfig::LongOption( "logtypes",          'L'),
        ArgConfig::LongOption( "nogo"                  ),
        ArgConfig::LongOption( "pidfile",           'P'),
        ArgConfig::LongOption( "protection",        'p'),
        ArgConfig::LongOption( "rewrite",           'r'),
        ArgConfig::LongOption( "socket",            's'),
        ArgConfig::LongOption( "user",              'u'),
        ArgConfig::LongOption( "version",           'v'),
    };
    ArgConfig::LongOption const *const longEnd =
                    longOptions +
                    sizeof(longOptions) / sizeof(ArgConfig::LongOption);
}

int main(int argc, char **argv)
try
{
    ArgConfig &arg = ArgConfig::initialize(
                                "c:d:e:g:hl:L:p:P:r:s:u:v", 
                                longOptions, longEnd, argc, argv);
    prepare(arg);

    Daemon daemon;
    daemon.execute();
}
catch (exception const &exc)
{
    string msg{ "Fatal error"s + exc.what() };
    cerr << msg << '\n';
    return 1;
}
catch (int i)
{
    return  ArgConfig::instance().option("hv") ? 0 : i;
}
