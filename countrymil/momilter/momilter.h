#ifndef _INCLUDED_MOMILTER_
#define _INCLUDED_MOMILTER_

#include <unordered_set>
#include <string>

#include <bobcat/milter>

#include "../judge/judge.h"
#include "../configlog/configlog.h"

class MoMilter: private ConfigLog, public FBB::Milter
{
    Judge &d_judge;

    static sfsistat s_action[];

    public:
        MoMilter(Judge &judge);
        ~MoMilter() override;

        void makeSocket();

    private:
        void setProtection(std::string const &socketName);
        size_t dottedDecimalToNumeric(std::string const &dotted) const;

        Milter *clone() const                                   override;
        sfsistat connect(char *hostname, _SOCK_ADDR *hostaddr)  override;
        sfsistat recipient(char **argv)                         override;
        sfsistat header(char *headerName, char *headerTxt)      override;
        sfsistat sender(char **argv)                            override;
        sfsistat close()                                        override;

        static size_t octalProtection(std::string const &protection);
        static void setPerms(std::string const &socketName, size_t mode);
};

//F        MoMilter(Database &db, CountryData &countryData);

//F    static FBB::Pattern s_ipAddress;
//F    static FBB::Pattern s_emptyHeader;
    
//F    // the Sender: is the From header
//F    static std::unordered_set<std::string> s_headerSet;


//F    using Key = std::pair<std::string, std::string>;

//F    Database d_db;
//F    CountryData d_countryData;

//F    Key d_key;
//F    std::string d_recipient;
//F    std::string d_connect;      //F if set then this connector only 
                                //F made a connect request.

//F    sfsistat (MoMilter::*d_unknownRecipient)();
//F    sfsistat (MoMilter::*d_reRejectSender)(char const *);


//F        sfsistat tempfailRecipient();
//F        sfsistat discardRecipient();
//F
//F        sfsistat received(std::string headerTxt) const;
//F        bool configuredReject(std::string const &ipAddress) const;
//F        bool rejectCountry(std::string const &ipAddress) const;
//F
//F
//F        sfsistat tempfailSender(char const *sender);
//F        sfsistat discardSender(char const *sender);

#endif
