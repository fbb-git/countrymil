#include "momilter.ih"

sfsistat MoMilter::sender(char **argv)
{
    if (*argv == 0)                                 // no 1st argument
    {
        log('i') << "Sender: " << Types::nr(id()) <<" NO SENDER" <<  endl;
        return CONTINUE;
    }
                                        // or return the judge's judgment
    return s_action[ d_judge.sender(id(), String::lc(*argv)) ];
}
