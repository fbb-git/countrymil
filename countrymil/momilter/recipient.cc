#include "momilter.ih"

sfsistat MoMilter::recipient(char **argv)
{
    if (*argv == 0)                                 // no 1st argument
    {
        log('i') << "Recipient: " << Types::nr(id()) << " NO NAME" <<  endl;
        return CONTINUE;
    }

                                        // or return the judge's judgment
    return s_action[ d_judge.recipient(id(), String::lc(*argv)) ];
}
