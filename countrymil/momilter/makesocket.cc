#include "momilter.ih"

void MoMilter::makeSocket()
{
    string const &socketName(config().socketName());

    log('s') << "Socket is `" << socketName << '\'' << endl;

    setConnection(socketName);
    if (!openSocket())
        throw Exception{} << "MoMilter::MoMilter()"
                ": can't open socket `" << socketName << '\'';

    setProtection(socketName);
}


