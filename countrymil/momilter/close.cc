#include "momilter.ih"

//override
sfsistat MoMilter::close()
{
    Types::Type action = d_judge.close(id());

    smfi_setpriv(id(), 0);

    return s_action[action];
}
