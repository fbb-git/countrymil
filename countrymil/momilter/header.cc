#include "momilter.ih"

// the Sender: is the From header

sfsistat MoMilter::header(char *headerName, char *headerTxt)
{
    if ("Received"s == headerName)          // merely log the received headers
        d_judge.received(id(), headerTxt);

    return CONTINUE;
}
