#include "momilter.ih"

    // caled by setProtection

// static
void MoMilter::setPerms(string const &socketName, size_t mode)
try
{
    fs::status(socketName).permissions(static_cast<fs::perms>(mode));
}
catch (...)
{
    throw Exception{} << "MoMilter::setPerms(): can't change the " 
                         "mode of " << socketName << " to " << oct << mode;
}
