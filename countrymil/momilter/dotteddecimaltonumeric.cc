#include "momilter.ih"

size_t MoMilter::dottedDecimalToNumeric(string const &dotted) const
{
    istringstream ins(dotted);
    size_t ip = 0;

    for (size_t octet, idx = 0; idx < 4; ++idx)
    {
        ins >> octet;
        (ip <<= 8) += octet;
        ins.ignore(1);
    }

    return ip;
}
