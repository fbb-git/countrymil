#include "momilter.ih"

// static
size_t MoMilter::octalProtection(string const &protection)
{
    istringstream istr(protection);

    size_t mode = 0;
    if (not (istr >> oct >> mode))              // not already octal: convert
    {
        try
        {
            if (protection.length() != 9)       // rwxrwxrwx: 9 chars
                throw false;

            mode = 0;
            for (size_t group = 0; group != 9; group += 3)
            {
                mode <<= 3;                     // prepare for the next spec.
                for (size_t who = 0; who != 3; ++who)
                {
                    switch (protection[group + who])    // ony r, w, - are OK
                    {
                        case 'r':
                            mode |= 4;
                        break;

                        case 'w':
                            mode |= 2;
                        break;

                        case '-':
                        break;

                        default:            // sockets aren't executable ('x')
                        throw false;        // or whatever
                    }
                }
            }
        }
        catch (bool error)
        {
            throw Exception{} << "MoMilter::setProtection()"
                ": ill formed protection `" << protection << '\'';
        }
    }
    return mode;
}
