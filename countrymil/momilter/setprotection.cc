#include "momilter.ih"

void MoMilter::setProtection(string const &socketName)
{                                                   // if a protection was
    if (                                            // specified then set the
        string const &protection = config().protection();   // socket's 
        not protection.empty()                              // protection
    )
        setPerms(socketName, octalProtection(protection));
}




