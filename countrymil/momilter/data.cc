#include "momilter.ih"

sfsistat MoMilter::s_action[] =      
{   
    ACCEPT,             // see types.h: sequence must match 
    CONTINUE,           //              Types::Type's sequence
    DISCARD, 
    REJECT,  
    TEMPFAIL
};
