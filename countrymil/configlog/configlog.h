#ifndef INCLUDED_CONFIGLOG_
#define INCLUDED_CONFIGLOG_

namespace FBB
{
    class Log;
}

class Config;

class ConfigLog
{
    Config const &d_config;
    FBB::Log &d_log;

    public:
        ConfigLog();

    protected:
        Config const &config() const;                   // .ih
        FBB::Log &log(char ch) const;                   // .ih
};
        
#endif
