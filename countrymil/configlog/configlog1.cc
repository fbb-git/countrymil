#define XERR
#include "configlog.ih"

ConfigLog::ConfigLog()
:
    d_config(Config::instance()),
    d_log(Log::instance())
{}
